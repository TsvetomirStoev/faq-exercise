import React from 'react'
import './question.css'
import Answer from '../answer-block/answer'

type QuestionProps = {
    questionNumber: Number,
    onClick: (e:any)=> void,
    answers: String,
    activeElement:Boolean
}

const Question = (props: QuestionProps) => {

    return (
        <React. StrictMode>
        <div className="btn-div" onClick={props.onClick} key={`question ${props.questionNumber}`}>
            <div className="question-block" id={`id ${props.questionNumber}`}>
                <p className="question-title" id={`id ${props.questionNumber}`}>
                    Question number {props.questionNumber}
                </p>
                <div  className="arrow-div" id={`id ${props.questionNumber}`}>
                <i className={`arrow-down ${props.activeElement ? 'clicked':''} `} id={`id ${props.questionNumber}`}></i>
                </div>
            </div>
            <Answer activeElement={props.activeElement} answerID={props.questionNumber} answerText={props.answers}/>
        </div>
        </React. StrictMode>
    )
}

export default Question;
