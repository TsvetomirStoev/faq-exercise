import React from 'react'
import './answer.css'

type AnswerProps = {
    answerText: String,
    answerID: Number,
    activeElement:Boolean
}

const answer = (props: AnswerProps) => {

    return (
        <div className={`answer-block ${props.activeElement ? 'clicked':''} `}  id={`id ${props.answerID}`}>
           <p className="answer-text" >{props.answerText}</p> 
        </div>
    )
}

export default answer
