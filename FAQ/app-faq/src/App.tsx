
import React, { useState} from 'react';
import './App.css';
import Question from './components/question_block/question';


const App = () =>  {

  const mock_answers: string[] = [
    "0 - Et quaerat, dolorum libero possimus quae aut rem blanditiis",
    "1 - Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
    "2 - Et quaerat, dolorum libero possimus quae aut rem blanditiis",
    "3 - impedit commodi itaque ipsam adipisci quam nemo. Veritatis",
    "4 - tempore quaerat obcaecati accusamus aut." ,
    "5 - Lorem ipsum, dolor sit amet consectetur adipisicing elit.",
    "6 - Et quaerat, dolorum libero possimus quae aut rem blanditiis",
    "7 - impedit commodi itaque ipsam adipisci quam nemo. Veritatis",
    "8 - tempore quaerat obcaecati accusamus aut." ,
    "9 - Lorem ipsum, dolor sit amet consectetur adipisicing elit."
  ];

  const[answer,setAnswer] = useState();
 
  const get_id = (index:any) =>{
    setAnswer(index);
  };


  return (
    <React. StrictMode>
    <div className="App">
     <h1 className="header">Freuiqntly asked questions</h1>
     <p className="description">Hello! Didn't find what you are looking for ? Please contact us.</p>

     {mock_answers.map( (element,index) => {
      return <Question 
      answers={index === answer ? element: ''}  
      key={index} 
      onClick={()=>{get_id(index)}} 
      questionNumber={index}
      activeElement={index === answer ? true: false}
      />
     })}

    </div>
    </React. StrictMode>
  );
}

export default App;
